import asyncio
import copy
import json
import logging
import signal
import sys
import time

from manager.datalake_manager import DataLakeManager
from manager.bus_manager import BusManager
from manager.geospatialdownloader_manager import GeospatialDownloaderManager
from manager.inference_manager import InferenceManager

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
logger = logging.getLogger(__name__)

geospatial_downloader_manager = GeospatialDownloaderManager()
inference_manager = InferenceManager()
data_lake_manager = DataLakeManager()

'''
Handle the full workflow that every message activates. It implies the following steps:

- STEP 1: Download the raw geotiff images of the area and period of interest using the geospatial_downloader_manager
- STEP 2 [Optional]: Upload the raw geotiff images to the data lake using the data_lake_manager
- STEP 3: Make the requested inference on the raw geotiff images and generate the prediction geotiff images using the
          inference_manager
- STEP 4: Upload the prediction geotiff images to the data lake using the data_lake_manager

Note that STEP 2 and (STEP 3 + STEP 4) are independent, hence can be executed in async
'''


async def work(message):
    try:
        message['ImagesSelectionCriterion'] = 'All'
        resources = await geospatial_downloader_manager.download_data(message)
        if resources is None:
            logger.info('No data available for the specified request.')
            return
        raw_data_dict = {
            'hazard': resources[0]['hazard'],
            'resources': resources
        }
        loop = asyncio.get_event_loop()
        tasks = [
            loop.create_task(data_lake_manager.upload_data(copy.deepcopy(raw_data_dict))),
            loop.create_task(inference_manager.get_inference(copy.deepcopy(raw_data_dict)))
        ]

        def internal_signal_handler(signal, frame):
            loop.stop()
            rabbitmq_consumer.stop()
            sys.exit(0)

        signal.signal(signal.SIGINT, internal_signal_handler)
        results = await asyncio.gather(*(task for task in tasks))
        uploaded_raw_data_info = results[0]
        if uploaded_raw_data_info[0]['package_id']:
            uploaded_data_info = await data_lake_manager.upload_data(
                results[1], metadata_id=uploaded_raw_data_info[0]['package_id'])
            message_dict = {'Success': True, 'Resources': uploaded_data_info, 'RequestCode': message['RequestCode']}
            logger.info('Sending result on output bus...')
            rabbit_producer.connect()
            rabbit_producer.publish(json.dumps(message_dict))
            rabbit_producer.disconnect()
            logger.info('Result sent on output bus.')
    except Exception as e:
        logger.error('Exception occurred during the message processing. Sending Failure message on bus.')
        try:
            message_dict = {'success': False, 'error': str(e)}
            rabbit_producer.connect()
            rabbit_producer.publish(json.dumps(message_dict))
            rabbit_producer.disconnect()
        except Exception as ee:
            logger.error(f'Failed to send Failure message on bus. Error: {str(ee)}')


def callback(message):
    try:
        start = time.perf_counter()
        asyncio.run(work(message))
        end = time.perf_counter() - start
        logger.info(f"Program finished in {end:0.2f} seconds.")
    except BaseException as e:
        logger.error('Exception encountered while handling the message: {}'.format(str(e)))
        # raise e <- forces the program to exit


'''
Listen to the RabbitMQ bus and execute the callback() method everytime a new message arrives
'''

if __name__ == "__main__":
    def signal_handler(signal, frame):
        rabbitmq_consumer.stop()
        sys.exit(0)


    signal.signal(signal.SIGINT, signal_handler)
    rabbit_producer = BusManager().get_output_producer()
    rabbitmq_consumer = BusManager().get_input_consumer(callback)
    rabbitmq_consumer.run()
