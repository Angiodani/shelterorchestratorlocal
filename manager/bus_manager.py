from driver.rabbitmq_driver import BasicProducer, JsonEncoder, DeliveryMode, Consumer
from manager import settings


class BusManager:

    def __init__(self):
        self.rabbitmq_host = settings.rabbitmq_host
        self.rabbitmq_port = settings.rabbitmq_port
        self.rabbitmq_user = settings.rabbitmq_user
        self.rabbitmq_pass = settings.rabbitmq_pass
        self.rabbitmq_vhost = settings.rabbitmq_vhost
        self.rabbitmq_input_routing = settings.broker_input_routing()
        self.rabbitmq_output_routing = settings.broker_output_routing()
        self.encoder = JsonEncoder()
        self.delivery_mode = DeliveryMode.PERSISTENT

    def get_input_producer(self):  # FOR TESTING PURPOSES
        rabbitmq_publisher = BasicProducer(self.rabbitmq_host, self.rabbitmq_port, self.rabbitmq_user,
                                           self.rabbitmq_pass, self.rabbitmq_vhost, self.rabbitmq_input_routing,
                                           self.encoder, self.delivery_mode)
        rabbitmq_publisher.connect()
        return rabbitmq_publisher

    def get_output_producer(self):
        rabbitmq_publisher = BasicProducer(self.rabbitmq_host, self.rabbitmq_port, self.rabbitmq_user,
                                           self.rabbitmq_pass, self.rabbitmq_vhost, self.rabbitmq_output_routing,
                                           self.encoder, self.delivery_mode)
        return rabbitmq_publisher

    def get_input_consumer(self, callback):
        rabbitmq_consumer = Consumer(self.rabbitmq_host, self.rabbitmq_port, self.rabbitmq_user,
                                     self.rabbitmq_pass, self.rabbitmq_vhost, callback)
        rabbitmq_consumer.configure(self.rabbitmq_input_routing)
        return rabbitmq_consumer
