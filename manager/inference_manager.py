import logging

from driver.inference_model_driver import prepare_input, prepare_output, run_inference
from utils import plot_image

logger = logging.getLogger(__name__)
SHOW = False


class InferenceManager:

    async def get_inference(self, raw_data_dict: dict) -> dict:
        logger.info('Inferencing...')
        if SHOW:
            for resource in raw_data_dict['resources']:
                image = resource['image']
                plot_image(image, factor=1)
        inference_input = prepare_input(raw_data_dict)
        inference_output = await run_inference(inference_input, raw_data_dict)
        inference_output = prepare_output(raw_data_dict, inference_output)
        if SHOW:
            for resource in inference_output['resources']:
                image = resource['image']
                plot_image(image)
        return inference_output
