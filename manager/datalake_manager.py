import logging

from driver.datalake_driver import get_access_token, create_metadata, upload_metadata, upload_resource, \
    delete_metadata, search_metadata, get_metadata

logger = logging.getLogger(__name__)


class DataLakeManager:

    async def upload_data(self, data: dict, metadata_id=None):
        logger.info('Getting access token...')
        token, refresh_token = await get_access_token()
        if metadata_id is None:
            metadata = create_metadata(data)
            metadata_id = await upload_metadata(metadata, access_token=token)
        logger.info(f'metadata_id: {metadata_id}')
        try:
            # iterate on resources and upload them one by one
            logger.info('Uploading resources...')
            for resource in data['resources']:
                success = await upload_resource(resource, metadata_id, access_token=token)
                if not success:
                    raise Exception
            logger.info('All resources successfully uploaded')
            return await self.get_data(metadata_id, access_token=token)
        except Exception as e:
            logger.error('One or more dataset uploads failed. Removing the metadata...')
            await self.delete_metadata(metadata_id, access_token=token)
            raise e

    async def search_metadata(self, start_date=None, end_date=None, datatype_id=None, include_private=True):
        try:
            body = {'extras': {}, 'fq_list': [], 'include_private': include_private}
            if start_date:
                body['extras']['ext_startdate'] = '{:%Y-%m-%dT%H:%M:%SZ}'.format(start_date)
            if end_date:
                body['extras']['ext_enddate'] = '{:%Y-%m-%dT%H:%M:%SZ}'.format(end_date)
            if datatype_id:
                body['fq_list'].append(f'datatype_id:{datatype_id}')
            token, refresh_token = await get_access_token()
            packages_result = await search_metadata(body, access_token=token)
            return packages_result
        except Exception:
            logger.error('There was an error while searching for metadata...')
            raise

    async def get_data(self, metadata_id, access_token=None) -> list:
        try:
            metadata = await get_metadata(metadata_id, access_token=access_token)
            return metadata['resources']
        except Exception as e:
            logger.error(f'There was an error while looking for data with metadata_id {metadata_id}')
            raise e

    async def delete_metadata(self, metadata_id, access_token=None):
        if metadata_id is None:
            logger.info('No metadata were uploaded, nothing to delete')
        logger.info(f'Deleting metadata with id {metadata_id}...')
        try:
            await delete_metadata(metadata_id, access_token)
        except Exception as e:
            logger.error(f'Error occurred while removing the metadata: {str(e)}')
            raise e
