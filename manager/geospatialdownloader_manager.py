import logging
import traceback
from typing import Tuple

import numpy as np
from datetime import datetime, timedelta
from sentinelhub import CRS, BBoxSplitter, bbox_to_dimensions
from shapely.geometry import shape

import driver.geospatialdownloader_internal_module_driver as gsdim
import driver.geospatialdownloader_external_module_driver as gsdem
from driver.sentinel_hub_layer import SentinelHubLayer
from manager import settings
from utils import plot_image

logger = logging.getLogger(__name__)

'''
    Retrieves the bounding box of the area of interest starting from polygon coordinates. If the area is too large,
    it is split into smaller areas. Returns the list of the bounding boxes and a tuple containing the widths and heights
    (in pixel) of the images of the areas, together with the specified resolution
    
    
    example of roi_coordinates dict:
    {
        "Resolution": 20,
        "BBox": {
            "type": "Polygon",
            "coordinates": [
                [[16.8791, 45.2966], [16.4012, 45.2966], [16.4012, 45.5391], [16.8791, 45.5391], [16.8791, 45.2966]]
            ]
        }
    }
'''


async def get_area_splits(crs: CRS, roi_coordinates: dict):
    geometry = shape(roi_coordinates['BBox']).buffer(0)
    bbox_splitter = BBoxSplitter([geometry], crs, (1, 1))
    width, height = bbox_to_dimensions(bbox_splitter.get_bbox_list()[0], resolution=roi_coordinates['Resolution'])
    if width < 480 or height < 480:
        if width > height:
            ratio = width / height
            height = 480
            width = height * ratio
        else:
            ratio = height / width
            width = 480
            height = width * ratio

    h_splits = int(np.ceil(width / settings.geospatial_downloader_max_image_dimension))
    v_splits = int(np.ceil(height / settings.geospatial_downloader_max_image_dimension))

    if h_splits > 1 or v_splits > 1:
        bbox_splitter = BBoxSplitter([geometry], crs, (h_splits, v_splits))

    bboxes = bbox_splitter.get_bbox_list()
    widths, heights = [], []
    for bbox in bboxes:
        w, h = bbox_to_dimensions(bbox, resolution=roi_coordinates['Resolution'])
        widths.append(w)
        heights.append(h)

    return bboxes, (widths, heights, roi_coordinates['Resolution'])


class GeospatialDownloaderManager:

    async def download_data(self, data_request: dict) -> [list, dict, None]:
        try:
            logger.info('Downloading geospatial images...')
            layers = SentinelHubLayer.get_WMSLayers(data_request['Hazard'])
            roi_coordinates = data_request['EventDelineationArea']['Geography']
            if 'Resolution' not in roi_coordinates:
                roi_coordinates['Resolution'] = 20
            if roi_coordinates['CoordinateReferenceSystem'] == 'WGS84':
                crs = CRS.WGS84
            elif roi_coordinates['CoordinateReferenceSystem'] == 'POP_WEB':
                crs = CRS.POP_WEB
            else:
                raise Exception(f'Coordinate Reference System {roi_coordinates["CoordinateReferenceSystem"]}'
                                f' not recognized. Use one of [WGS84, POP_WEB]')
            roi_bboxes, whr = await get_area_splits(crs, roi_coordinates)
            logger.info(f'Looking for images at {roi_coordinates["Resolution"]}m resolution')
            logger.info(f'Identified {len(whr[0])} area{"s" if len(whr[0]) > 1 else ""} that span(s) the'
                        f' entire geometry to be mapped')
            for i in range(len(whr[0])):
                logger.info(f'Area {i} dimensions: Width {whr[0][i]} - Height {whr[1][i]}')
            time_from = data_request['EventDelineationTime']
            max_delta_time = data_request['EventDelineationTimeMaxDelta'] if data_request[
                'EventDelineationTimeMaxDelta'] else 0

            images, dates = await self.get_raw_data(roi_bboxes, whr, layers, time_from, max_delta_time, 'Earliest')
            if images is None or len(images) == 0:
                return None

            raw_data = []
            for idx, (img, date) in enumerate(zip(images, dates)):
                # image_coords contains the coordinates of the geospatial image. If they are not present, this field
                # remains None and the image will use the coordinates of the mapping request (should always be present
                # or problems may rise when splitting the map request area in smaller areas or when the actual image
                # is not precisely aligned with the map request area)
                image_coords = None
                if isinstance(img, tuple):
                    image_coords = img[1]
                    assert isinstance(image_coords, list)
                    img = img[0]
                    assert isinstance(img, np.ndarray)
                datum = {
                    'hazard': data_request['Hazard'],
                    'title': data_request['Title'],
                    'notes': data_request['Notes'],
                    'name_prefix': f'geospatial_image_{idx}',
                    'format': 'tiff',
                    'data_temporal_extent_begin_date': date,
                    'data_temporal_extent_end_date': date,
                    "coordinate_system_id": roi_coordinates['CoordinateSystemId'],
                    "coordinate_reference_system": roi_coordinates['CoordinateReferenceSystem'],
                    'bbox': roi_coordinates['BBox'],
                    'resolution': roi_coordinates['Resolution'],
                    'image': img,
                    'image_coords': image_coords
                }
                raw_data.append(datum)

            logger.info('Data downloaded.')
            if data_request['ImagesSelectionCriterion'] == 'All':
                return raw_data
            if data_request['ImagesSelectionCriterion'] == 'Earliest':
                return raw_data[0]
            if data_request['ImagesSelectionCriterion'] == 'Latest':
                return raw_data[-1]
            raise Exception(
                f'Field "ImagesSelectionCriterion" of data request is invalid: '
                f'{data_request["ImagesSelectionCriterion"]}. Use one of the following: All, Earliest, Latest')
        except Exception as e:
            logger.error(f'Error occurred while downloading the geospatial data: {traceback.format_exc()}')
            raise e

    async def get_raw_data(self, roi_bboxes, whr, layers, time_from, max_delta_time, criterion) \
            -> [Tuple[list, list], Tuple[None, None]]:
        if criterion == 'All':
            time_to = self.add_days(time_from, max_delta_time)
            logger.info(f'Looking for images from {time_from} to {time_to}')
            return await self.get_raw_data_in_range(roi_bboxes, whr, layers, time_from, time_to)
        if criterion == 'Earliest':
            # Look for images in the time range starting from the earliest date and moving one day at a time forward.
            # The first image available is returned as the result
            for time_distance in range(max_delta_time + 1):
                time_from_actual = self.add_days(time_from, time_distance)
                time_to_actual = self.add_days(time_from_actual, 1)
                logger.info(f'Looking for images from {time_from_actual} to {time_to_actual}')
                images, dates = await self.get_raw_data_in_range(roi_bboxes, whr, layers, time_from_actual,
                                                                 time_to_actual)
                if images is not None and len(images) > 0:
                    return images, dates
        if criterion == 'Latest':
            # Look for images in the time range starting from the latest date and moving one day at a time backward.
            # The first image available is returned as the result
            time_to = self.add_days(time_from, max_delta_time)
            for time_distance in range(max_delta_time + 1):
                time_to_actual = self.add_days(time_to, -time_distance)
                time_from_actual = self.add_days(time_to_actual, -1)
                logger.info(f'Looking for images from {time_from_actual} to {time_to_actual}')
                images, dates = await self.get_raw_data_in_range(roi_bboxes, whr, layers, time_from_actual,
                                                                 time_to_actual)

                if images is not None and len(images) > 0:
                    return images, dates
        return None, None

    async def get_raw_data_in_range(self, roi_bboxes, whr, layers, time_from, time_to) \
            -> [Tuple[list, list], Tuple[None, None]]:
        for layer in layers:
            if settings.geospatial_downloader_use_external_driver:
                requests = await gsdem.prepare_wms_request(roi_bboxes, whr, layer, time_from, time_to)
                images, dates = await gsdem.download_data(requests)
            else:
                requests = await gsdim.prepare_wms_request(roi_bboxes, whr, layer, time_from, time_to)
                images, dates = await gsdim.download_data(requests)
            if images is None:
                continue
            return images, dates
        return None, None

    def add_days(self, date, days):
        date_format = '%Y-%m-%d'
        date = datetime.strptime(date, date_format)
        date = date + timedelta(days=days)
        date = date.strftime(date_format)
        return date
