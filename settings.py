import os

from pydantic import BaseSettings

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


class AppSettings(BaseSettings):
    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'

    rabbitmq_host: str
    rabbitmq_port: str
    rabbitmq_user: str
    rabbitmq_pass: str
    rabbitmq_vhost: str

    rabbitmq_exchange: str
    rabbitmq_exchange_type: str
    rabbitmq_input_queue: str
    rabbitmq_input_routing_key: str  # FOR TESTING PURPOSES -> USED BY A PRODUCER FOR TESTING PURPOSES
    rabbitmq_output_routing_key: str

    geospatial_downloader_use_external_driver: bool
    geospatial_downloader_oauth_client_id: str
    geospatial_downloader_oauth_client_secret: str
    geospatial_downloader_instance_id: str
    geospatial_downloader_data_folder: str
    geospatial_downloader_max_image_dimension: int = 5000

    external_geospatial_downloader_base_url: str

    inference_framework_host: str
    inference_framework_port: str
    inference_framework_flood_detection_service_name: str
    inference_framework_fire_detection_service_name: str

    data_lake_flood_metadata_datatype_id: str
    data_lake_fire_metadata_datatype_id: str
    data_lake_flood_metadata_topic_category: str
    data_lake_fire_metadata_topic_category: str
    data_lake_flood_metadata_keyword_value: str
    data_lake_fire_metadata_keyword_value: str
    organization_name: str
    organization_email: str

    def get_external_geospatial_downloader_sentinel1_url(self):
        return f'{self.external_geospatial_downloader_base_url}/sentinel1acquisition'

    def get_external_geospatial_downloader_sentinel2_url(self):
        return f'{self.external_geospatial_downloader_base_url}/sentinel2acquisition'

    def get_inference_framework_detection_service_name(self, hazard: str):
        if hazard == 'Flood':
            return self.inference_framework_flood_detection_service_name
        if hazard == 'Wildfire':
            return self.inference_framework_fire_detection_service_name
        return None

    def get_data_lake_metadata_datatype_id(self, hazard: str):
        if hazard == 'Flood':
            return self.data_lake_flood_metadata_datatype_id
        if hazard == 'Wildfire':
            return self.data_lake_fire_metadata_datatype_id
        return None

    def get_data_lake_metadata_topic_category(self, hazard: str):
        if hazard == 'Flood':
            return self.data_lake_flood_metadata_topic_category
        if hazard == 'Wildfire':
            return self.data_lake_fire_metadata_topic_category
        return None

    def get_data_lake_metadata_keyword_value(self, hazard: str):
        if hazard == 'Flood':
            return self.data_lake_flood_metadata_keyword_value
        if hazard == 'Wildfire':
            return self.data_lake_fire_metadata_keyword_value
        return None

    ckan_url: str
    oauth_url: str
    oauth_api_key: str
    oauth_app_id: str
    oauth_user: str
    oauth_pwd: str

    metadata_filename: str

    temp_folder: str = ROOT_DIR + '\\temp\\'

    def broker_url(self):
        return f"amqp://{self.rabbitmq_user}:{self.rabbitmq_pass}@{self.rabbitmq_host}:{self.rabbitmq_port}/{self.rabbitmq_vhost}"

    def broker_input_routing(self):
        return {
            'exchange': self.rabbitmq_exchange,
            'exchange_type': self.rabbitmq_exchange_type,
            'queue': self.rabbitmq_input_queue,
            'routing_key': self.rabbitmq_input_routing_key,  # FOR TESTING PURPOSES -> USED BY A PRODUCER FOR TESTING PURPOSES
        }

    def broker_output_routing(self):
        return {
            'exchange': self.rabbitmq_exchange,
            'exchange_type': self.rabbitmq_exchange_type,
            'routing_key': self.rabbitmq_output_routing_key,
        }

    def inference_framework_url(self):
        return f'http://{self.inference_framework_host}:{self.inference_framework_port}'

    def oauth_login_url(self):
        return f'{self.oauth_url}/api/login'

    def oauth_headers(self):
        return {
            "Authorization": self.oauth_api_key,
        }

    def oauth_body(self):
        return {
            "loginId": self.oauth_user,
            "password": self.oauth_pwd,
            "applicationId": self.oauth_app_id,
            "noJWT": False
        }

    def data_lake_package_url(self):
        return f'{self.ckan_url}/api/action/package_create'

    def data_lake_resource_url(self):
        return f'{self.ckan_url}/api/action/resource_create'

    def data_lake_package_search_url(self):
        return f'{self.ckan_url}/api/action/package_search'

    def data_lake_package_show_url(self):
        return f'{self.ckan_url}/api/action/package_show'

    def data_lake_delete_metadata_url(self):
        return f'{self.ckan_url}/api/action/package_delete'

    @staticmethod
    def data_lake_headers(access_token):
        return {
            "Authorization": f'Bearer {access_token}',
        }
