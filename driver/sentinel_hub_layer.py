from enum import Enum

from sentinelhub import DataCollection


class SentinelHubLayer(Enum):
    eval_scripts = {
        'flood': '''
            function setup() {
              return {
                input: [{
                    bands: ["VV", "VH"]
                }],
                output: { bands: 3 }
              }
            }
            
            function evaluatePixel(sample) {
                return [sample.VV, 2 * sample.VH, sample.VV / sample.VH / 100.0];
            }
        ''',
        'fire': '''
            function setup() {
              return {
                input: [{
                    bands: ["B01", "B02", "B03", "B04", "B05", "B06", "B07", "B08", "B8A", "B09", "B11", "B12"]
                }],
                output: { bands: 12 }
              }
            }
            
            function evaluatePixel(sample) {
                return [sample.B01, sample.B02, sample.B03, sample.B04, sample.B05, sample.B06, sample.B07, sample.B08, sample.B8A, sample.B09, sample.B11, sample.B12];
            }
        ''',
        'cloud_coverage': '''
            function setup() {
              return {
                input: ["CLM"],
                output: { bands: 1 }
              }
            }
        
            function evaluatePixel(sample) {
              if (sample.CLM == 1) {
                return [1]
              } 
              return [0];
            }
            '''
    }

    WMS_FLOOD = DataCollection.SENTINEL1_IW, 'FLOOD', None
    WMS_FIRE_L2A = DataCollection.SENTINEL2_L2A, 'FIRE-L2A', 0.2
    WMS_FIRE_L1C = DataCollection.SENTINEL2_L1C, 'FIRE-L1C', 0.2
    WMS_CLOUD_COVERAGE = DataCollection.SENTINEL2_L2A, 'CLOUD-COVERAGE'

    API_FLOOD = DataCollection.SENTINEL1_IW, eval_scripts['flood'], None
    API_FIRE_L2A = DataCollection.SENTINEL2_L2A, eval_scripts['fire'], 0.2
    API_FIRE_L1C = DataCollection.SENTINEL2_L1C, eval_scripts['fire'], 0.2
    API_CLOUD_COVERAGE = DataCollection.SENTINEL2_L2A, eval_scripts['cloud_coverage']

    @classmethod
    def get_WMSLayers(cls, hazard: str):
        # There may be multiple layers, this means that if no data is found using the first layer, the second will be
        # used, and so on
        switcher = {
            'Flood': [cls.WMS_FLOOD],
            'Wildfire': [cls.WMS_FIRE_L1C]
        }

        return switcher.get(hazard, None)

    @classmethod
    def get_APILayers(cls, hazard: str):
        # There may be multiple layers, this means that if no data is found using the first layer, the second will be
        # used, and so on
        switcher = {
            'Flood': [cls.API_FLOOD],
            'Wildfire': [cls.API_FIRE_L2A, cls.API_FIRE_L1C]
        }

        return switcher.get(hazard, None)
