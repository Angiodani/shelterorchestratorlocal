import datetime
import logging
import traceback
from io import BytesIO
import numpy as np
import requests
import tifffile
from sentinelhub import SHConfig, DataCollection
from manager import settings

logger = logging.getLogger(__name__)


async def prepare_wms_request(roi_bboxes, whr, layer, time_from, time_to=None):
    config = SHConfig()
    config.instance_id = settings.geospatial_downloader_instance_id
    _, _, resolution = whr
    wms_requests = []
    for i in range(len(roi_bboxes)):
        aoi = ','.join([str(coord) for coord in roi_bboxes[i]])
        request_body = dict(
                aoi=aoi,
                from_date=time_from,
                to_date=time_to,
                resolution=resolution,
                max_cc=(100.0 - 10*layer.value[2]) if layer.value[2] is not None else None
            )
        wms_requests.append(request_body)
    return wms_requests, layer.value[0], layer.value[1]


async def download_data(requests_tuple: tuple):
    requests_list, data_collection, collection_id = requests_tuple
    images, dates = [], []
    try:
        if data_collection == DataCollection.SENTINEL1_IW:
            url = settings.get_external_geospatial_downloader_sentinel1_url()
        else:
            url = settings.get_external_geospatial_downloader_sentinel2_url()
        for request_data in requests_list:
            response = requests.post(url, data=request_data)
            if response.status_code == 200:
                logger.info(f'Image found')
                # Store binary data in a buffer
                bytes_data = BytesIO(response.content)
                # Open the buffered data as a tif image
                with tifffile.TiffFile(bytes_data) as tif:
                    xmin, ymin, xres, yres = 0, 0, 0, 0
                    image = tif.pages[0].asarray()
                    # Extract the tags of the image containing the bbox bottom-left coordinate (in WGS84 coordinate
                    # system) and the spatial resolution of the image
                    for tag in tif.pages[0].tags.values():
                        if tag.name == 'ModelTiepointTag':
                            _, _, _, xmin, ymin, _ = tag.value
                        if tag.name == 'ModelPixelScaleTag':
                            xres, yres, _ = tag.value
                    coordinates_list = [xmin, ymin, xres, yres]
                img_tuple = image, coordinates_list
                images.append(img_tuple)
                # Use the request date (at 00:00) as the result date.
                # TODO can geospatial downloader return the precise acquisition date of the image?
                dates.append(datetime.datetime.strptime(request_data['from_date'], '%Y-%m-%d'))
            else:
                logger.warning(f'Returned status code {response.status_code}. Message: {response.text}')
        if len(images) == 0:
            return None, None
        return images, np.array(dates)
    except Exception as e:
        logger.error(traceback.format_exc())
        raise e
