import datetime
import logging
import numpy as np

from sentinelhub import SHConfig, CustomUrlParam
from sentinelhub import WmsRequest, MimeType

from driver.sentinel_hub_layer import SentinelHubLayer
from manager import settings
from utils import plot_image

logger = logging.getLogger(__name__)

'''
https://sentinelhub-py.readthedocs.io/en/latest/examples/ogc_request.html#Imports
'''


async def prepare_wms_request(roi_bboxes, whr, layer, time_from, time_to=None, time_difference_hours=12):
    config = SHConfig()
    config.instance_id = settings.geospatial_downloader_instance_id
    widths, heights, _ = whr
    wms_requests = []
    wms_clouds_requests = []
    for i in range(len(roi_bboxes)):
        try:
            dc = layer.value[0]
            # dc.service_url = ServiceUrl.EOCLOUD <- non funziona
            wms_request = WmsRequest(
                data_collection=dc,
                layer=layer.value[1],
                bbox=roi_bboxes[i],
                time=time_from if time_to is None else (time_from, time_to),
                width=widths[i],
                height=heights[i],
                config=config,
                time_difference=datetime.timedelta(hours=time_difference_hours),
                custom_url_params={
                    CustomUrlParam.TRANSPARENT: True,
                    CustomUrlParam.QUALITY: 100,
                    CustomUrlParam.SHOWLOGO: False
                },
                image_format=MimeType.TIFF_d32f,
                data_folder=settings.geospatial_downloader_data_folder
            )
            wms_requests.append(wms_request)
            wms_clouds_request = (WmsRequest(
                data_collection=SentinelHubLayer.WMS_CLOUD_COVERAGE.value[0],
                layer=SentinelHubLayer.WMS_CLOUD_COVERAGE.value[1],
                bbox=roi_bboxes[i],
                time=time_from if time_to is None else (time_from, time_to),
                width=widths[i],
                height=heights[i],
                config=config,
                time_difference=datetime.timedelta(hours=time_difference_hours),
                custom_url_params={
                    CustomUrlParam.TRANSPARENT: True,
                    CustomUrlParam.SHOWLOGO: False,
                },
                image_format=MimeType.TIFF,
                data_folder=settings.geospatial_downloader_data_folder
            ), layer.value[2]) if layer.value[2] is not None else None
            wms_clouds_requests.append(wms_clouds_request)
        except Exception as e:
            logger.error(f'Error while preparing the request for downloading geospatial data: {str(e)}')
            raise
    return wms_requests, wms_clouds_requests


async def download_data(requests_tuple: tuple):
    all_images = None
    all_dates = None
    requests, clouds_request_tuples = requests_tuple
    for request, clouds_request_tuple in zip(requests, clouds_request_tuples):
        clouds_request, max_cloud_coverage = clouds_request_tuple if clouds_request_tuple is not None else (None, None)
        try:
            dates = np.array(request.get_dates())
            if dates is None or len(dates) == 0:
                logger.info('No images found for the requested dates')
                return None, None
            cloud_coverage_compliance = np.array([], dtype=np.bool)
            images = np.array(request.get_data(save_data=True))
            if images.size == 0 or np.all(images[0] == 0):
                return None, None
            if clouds_request is not None:
                logger.info(f'Checking images cloud coverage')
                clouds = clouds_request.get_data(save_data=True)
                for idx, img in enumerate(clouds):
                    img = img / 255
                    logger.info(f'image cloud coverage: {np.mean(img)}')
                    # plot_image(img)
                    cloud_coverage_compliance = np.append(cloud_coverage_compliance, np.mean(img) <= max_cloud_coverage)
                if not np.any(cloud_coverage_compliance):
                    logger.info(f'None of the images has a cloud coverage <= {max_cloud_coverage}')
                    return None, None
                images = images[cloud_coverage_compliance]
                dates = dates[cloud_coverage_compliance]
            if all_images is None:
                all_images = images
                all_dates = dates
            else:
                all_images = np.append(all_images, images, axis=0)
                all_dates = np.append(all_dates, dates)
        except Exception as e:
            logger.error(str(e))
    return all_images, all_dates


'''
https://sentinelhub-py.readthedocs.io/en/latest/examples/processing_api_request.html

async def prepare_api_request(roi_coordinates, layer, time_from, time_to=None):
    config = SHConfig()
    config.sh_client_id = settings.geospatial_downloader_oauth_client_id
    config.sh_client_secret = settings.geospatial_downloader_oauth_client_secret
    # config.sh_base_url = 'https://services.eocloud.sentinel-hub.com/v1/'
    if roi_coordinates['CoordinateReferenceSystem'] == 'WGS84':
        crs = CRS.WGS84
    elif roi_coordinates['CoordinateReferenceSystem'] == 'POP_WEB':
        crs = CRS.POP_WEB
    else:
        raise Exception(f'Field "CoordinateReferenceSystem" of RoI coordinates is invalid: '
                        f'{roi_coordinates["CoordinateReferenceSystem"]}. Use one of the following: WGS84, POP_WEB')
    roi_bbox = BBox(bbox=roi_coordinates['BBox'], crs=crs)
    resolution = roi_coordinates['Resolution']
    roi_size = bbox_to_dimensions(roi_bbox, resolution=resolution)
    logger.info(f'Image shape at {resolution} m resolution: {roi_size} pixels')
    logger.info(f'Looking for images from {time_from} to {time_to} on layer {layer.value[0]}')
    try:
        data_collection = layer.value[0]
        in_data = SentinelHubRequest.input_data(
            data_collection=data_collection,
            time_interval=(time_from, time_to),
        )
        api_request = SentinelHubRequest(
            evalscript=layer.value[1],
            input_data=[
                in_data
            ],
            responses=[
                SentinelHubRequest.output_response('default', MimeType.TIFF)
            ],
            bbox=roi_bbox,
            size=roi_size,
            config=config,
            data_folder=settings.geospatial_downloader_data_folder
        )
        # api_request.download_list = [DownloadRequest(
        #    request_type=RequestType.POST,
        #    url='https://service.eocloud.sentinel-hub.com/api/v1/process',
        #    post_values=api_request.payload,
        #    data_folder=api_request.data_folder,
        #    save_response=bool(api_request.data_folder),
        #    data_type=api_request.mime_type,
        #    headers={'content-type': MimeType.JSON.get_string(), 'accept': api_request.mime_type.get_string()},
        #    use_session=True
        # )]
        clouds_api_request = (SentinelHubRequest(
            evalscript=SentinelHubLayer.API_CLOUD_COVERAGE.value[1],
            input_data=[
                SentinelHubRequest.input_data(
                    data_collection=SentinelHubLayer.API_CLOUD_COVERAGE.value[0],
                    time_interval=(time_from, time_to)
                )
            ],
            responses=[
                SentinelHubRequest.output_response('default', MimeType.TIFF)
            ],
            bbox=roi_bbox,
            size=roi_size,
            config=config,
            data_folder=settings.geospatial_downloader_data_folder
        ), layer.value[2]) if layer.value[2] is not None else None
        return api_request, clouds_api_request
    except Exception as e:
        logger.error(f'Error while preparing the request for downloading geospatial data: {str(e)}')
        raise
'''