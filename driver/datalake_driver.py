import json
import logging
import traceback
import uuid
from datetime import datetime

import aiohttp
import numpy as np
from aiohttp.web import HTTPError

from manager import settings
from utils import save_array_as_geotiff, clear_dir

logger = logging.getLogger(__name__)


async def get_access_token() -> tuple:
    url = settings.oauth_login_url()
    body = settings.oauth_body()
    headers = settings.oauth_headers()
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, json=body, headers=headers) as response:
                response.raise_for_status()
                if response.status == 200:
                    resp_as_dict = await response.json()
                    token, refresh_token = resp_as_dict["token"], resp_as_dict["refreshToken"]
                    logger.info("Access Token obtained")
                    return token, refresh_token
                logger.error(await response.json())
                raise Exception
    except HTTPError as http_err:
        logger.error(f"HTTP error occurred: {http_err}")
    except Exception as err:
        logger.error(f"Other error occurred: {err}")


def create_metadata(data: dict):
    logger.info('Creating metadata...')
    try:
        with open(settings.metadata_filename, "r", encoding='UTF-8') as f:
            metadata = json.load(f)
        metadata['title'] = f'Rapid Damage Assessment: {data["resources"][0]["title"]}'
        metadata['notes'] = f'{data["resources"][0]["notes"]}'
        metadata['datatype_id'] = settings.get_data_lake_metadata_datatype_id(data['hazard'])
        metadata['name'] = str(uuid.uuid4())
        metadata['classification_TopicCategory'] = settings.get_data_lake_metadata_topic_category(data['hazard'])
        metadata['keyword_KeywordValue'] = settings.get_data_lake_metadata_keyword_value(data['hazard'])
        # Use the bbox of the first resource as the package bbox
        metadata['spatial'] = data['resources'][0]['bbox']
        metadata['spatial'] = str(metadata['spatial']).replace("'", '"')
        metadata['temporalReference_dateOfPublication'] = '{:%Y-%m-%dT%H:%M:%S}'.format(datetime.utcnow())
        metadata['temporalReference_dateOfLastRevision'] = '{:%Y-%m-%dT%H:%M:%S}'.format(datetime.utcnow())
        # Use the date of the first resource as the begin and end date of the event
        metadata['data_temporal_extent_begin_date'] = str(
            data['resources'][0]['data_temporal_extent_begin_date']).replace(' ', 'T')
        metadata['data_temporal_extent_end_date'] = str(data['resources'][0]['data_temporal_extent_end_date']).replace(
            ' ', 'T')
        metadata['temporalReference_date'] = str(data['resources'][0]['data_temporal_extent_begin_date']).replace(' ',
                                                                                                                  'T')
        metadata['temporalReference_dateOfCreation'] = '{:%Y-%m-%dT%H:%M:%S}'.format(datetime.utcnow())
        metadata['quality_and_validity_spatial_resolution_latitude'] = data['resources'][0]['resolution']
        metadata['quality_and_validity_spatial_resolution_longitude'] = data['resources'][0]['resolution']
        metadata['responsable_organization_name'] = settings.organization_name
        metadata['responsable_organization_email'] = settings.organization_email
        metadata['point_of_contact_name'] = settings.organization_name
        metadata['point_of_contact_email'] = settings.organization_email
        logger.info('Metadata created...')
        return metadata
    except Exception as e:
        logger.error(traceback.format_exc())
        raise


async def upload_metadata(body, access_token=None):
    logger.info('Uploading metadata...')
    url = settings.data_lake_package_url()
    access_token = (await get_access_token())[0] if access_token is None else access_token
    headers = settings.data_lake_headers(access_token)
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, json=body, headers=headers) as response:
                if response.status == 200:
                    response_dict = await response.json()
                    assert response_dict["success"] is True
                    created_package = response_dict["result"]
                    logger.info("Metadata uploaded")
                    return created_package["id"]
                raise Exception(await response.json())
    except HTTPError as http_err:
        logger.error(f"HTTP error occurred: {http_err}")
    except Exception as err:
        logger.error(f"Other error occurred: {err}")


async def upload_resource(resource, metadata_id, access_token=None) -> bool:
    logger.info('Uploading resource...')
    url = settings.data_lake_resource_url()
    access_token = (await get_access_token())[0] if access_token is None else access_token
    headers = settings.data_lake_headers(access_token)
    resource_body = {
        "package_id": metadata_id,
        "name": f'{resource["name_prefix"] if resource["name_prefix"] else "resource"}_'
                f'{str(resource["data_temporal_extent_begin_date"]).replace(" ", "T").replace("-", "").replace(":", "")}_'
                f'{str(resource["data_temporal_extent_end_date"]).replace(" ", "T").replace("-", "").replace(":", "")}.{resource["format"]}',
        "format": resource['format'],
        "file_date_start": str(resource['data_temporal_extent_begin_date']).replace(' ', 'T'),
        "file_date_end": str(resource['data_temporal_extent_end_date']).replace(' ', 'T')
    }
    try:
        if isinstance(resource['image'], str):
            filename = resource['image']
        elif isinstance(resource['image'], np.ndarray):
            filename = save_array_as_geotiff(resource['image'],
                                             resource['bbox'] if resource['image_coords'] is None else
                                             resource['image_coords'],
                                             resource['coordinate_system_id'],
                                             resource_body['name'])
        else:
            raise Exception(f'resource type not supported for upload: {type(resource["image"])}')
    except Exception as e:
        logger.error(traceback.format_exc())
        return False
    try:
        with open(filename, 'rb') as file:
            resource_body['upload'] = file
            async with aiohttp.ClientSession() as session:
                async with session.post(url,
                                        data=resource_body,
                                        headers=headers) as response:
                    if response.status != 200:
                        logger.error(await response.json())
                        return False
                    logger.info('Resource uploaded...')
                    return True
    except Exception as e:
        logger.error(traceback.format_exc())
        return False
    finally:
        clear_dir('\\'.join(filename.split('\\')[:-1]))


async def get_metadata(metadata_id, access_token=None):
    logger.info(f'Getting metadata with id {metadata_id}')
    url = settings.data_lake_package_show_url()
    access_token = (await get_access_token())[0] if access_token is None else access_token
    headers = settings.data_lake_headers(access_token)
    body = {'id': metadata_id}
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, json=body, headers=headers) as response:
                if response.status == 200:
                    response_dict = await response.json()
                    assert response_dict["success"] is True
                    result = response_dict["result"]
                    logger.info("Metadata uploaded")
                    return result
                raise Exception(await response.json())
    except HTTPError as http_err:
        logger.error(f"HTTP error occurred: {http_err}")
    except Exception as err:
        logger.error(f"Other error occurred: {err}")


async def search_metadata(body, access_token=None):
    logger.info('Looking for data: ...')
    url = settings.data_lake_package_search_url()
    access_token = (await get_access_token())[0] if access_token is None else access_token
    headers = settings.data_lake_headers(access_token)
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, json=body, headers=headers) as response:
                if response.status == 200:
                    response_dict = await response.json()
                    assert response_dict["success"] is True
                    packages_result = response_dict["result"]
                    logger.info("Metadata uploaded")
                    return packages_result
                raise Exception(await response.json())
    except HTTPError as http_err:
        logger.error(f"HTTP error occurred: {http_err}")
    except Exception as err:
        logger.error(f"Other error occurred: {err}")


async def delete_metadata(metadata_id, access_token=None):
    url = settings.data_lake_delete_metadata_url()
    access_token = (await get_access_token())[0] if access_token is None else access_token
    headers = settings.data_lake_headers(access_token)
    body = {"id": metadata_id}
    try:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, json=body, headers=headers) as response:
                if response.status == 200:
                    response_dict = await response.json()
                    assert response_dict["success"] is True
                    logger.info('Metadata deleted')
                raise Exception(await response.json())
    except HTTPError as http_err:
        logger.error(f"HTTP error occurred: {http_err}")
    except Exception as err:
        logger.error(f"Other error occurred: {err}")


def bbox_to_geometry(bbox: list) -> dict:
    return {
        'type': 'MultiPolygon',
        'coordinates': [
            [
                [
                    bbox[:2],
                    [bbox[0], bbox[3]],
                    bbox[2:],
                    [bbox[2], bbox[1]],
                    bbox[:2]
                ]
            ]
        ]
    }
