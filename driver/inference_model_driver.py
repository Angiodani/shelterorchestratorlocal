import asyncio
import copy
import io
import logging
import os
import traceback
import aiohttp
from PIL import Image
import numpy as np
from manager import settings
from utils import get_image_crops, get_image_from_crops

logger = logging.getLogger(__name__)


def prepare_input(raw_data_dict: dict):
    logger.info('Tiling input images')
    try:
        image_crops_list = []
        for raw_resource in raw_data_dict['resources']:
            raw_image = raw_resource['image']
            image_crops = get_image_crops(raw_image)
            image_crops_list.append(image_crops)
        return image_crops_list
    except Exception as e:
        logger.error(traceback.format_exc())
        raise e


async def get_inference_result(inference_id):
    async with aiohttp.ClientSession() as session:
        async with session.get(f'{settings.inference_framework_url()}/results/{inference_id}/files') as response:
            if response.status == 200:
                pil_image = Image.open(io.BytesIO(await response.content.read()))
                return np.array(pil_image)
            if response.status == 404:
                detail = (await response.json())['detail']
                if 'failed' not in detail:
                    await asyncio.sleep(1)
                    print(detail)
                    return await get_inference_result(inference_id)
    raise Exception(f'Failed to retrieve inference result: {await response.json()}')


async def run_inference(input_data, raw_data_dict) -> list:
    hazard = raw_data_dict['hazard']
    logger.info("Running inference...")
    try:
        # Get the id of the inference service
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{settings.inference_framework_url()}/services?'
                                   f'names={settings.get_inference_framework_detection_service_name(hazard)}'
                                   f'&statuses=active') as response:
                services_list = await response.json()
                if len(services_list) == 0:
                    raise Exception(
                        f'Service "{settings.get_inference_framework_detection_service_name(hazard)}" '
                        f'is not running. Could not run inference.')
                service_id = services_list[0]['id']
        inference_id_dict = {}
        crops_counter = 0
        for index_image, image_crops in enumerate(input_data):
            for index_crop, image_crop in enumerate(image_crops):
                # Save image on buffer and attach it to the post request as a file
                buf, extension = save_on_buffer(hazard, image_crop)
                '''
                USE THIS CODE FOR RUNNING INFERENCE IN SYNC MODE
                file = dict(files=(f'crop_{index_crop}.{get_format_from_hazard(hazard)}', buf, "multipart/form-data"))
                data = None
                # Run inference and save the inference id
                response = requests.post(f'{settings.inference_framework_url()}/predictions/{service_id}',
                                         data=data, files=file)
                inference_id_dict[(index_image, index_crop)] = response.json()['inference_id']
                '''

                temporary_location = f'crop_{index_crop}.{extension}'
                with open(temporary_location, 'wb') as temp:
                    temp.write(buf.getvalue())
                with open(temporary_location, 'rb') as f:
                    data = {
                        'files': f,
                        'data': get_inference_request_data(hazard)
                    }
                    # Run inference and save the inference id
                    async with aiohttp.ClientSession() as session:
                        async with session.post(f'{settings.inference_framework_url()}/predictions/{service_id}',
                                                data=data) as response:
                            resp = await response.json()
                            inference_id_dict[(index_image, index_crop)] = resp['inference_id']
                            crops_counter += 1
                            logger.info(await response.json())
                os.remove(temporary_location)

        logger.info("Retrieving inference results...")
        inference_results = {}
        inference_results_2 = {}
        while crops_counter > 0:
            for key in inference_id_dict.keys():
                if inference_id_dict[key] > -1:
                    result_crop = await get_inference_result(inference_id_dict[key])
                    if hazard == 'Wildfire':
                        result_crop_2 = await get_inference_result(f'{str(inference_id_dict[key])}:1')
                        inference_results_2[key] = result_crop_2
                    logger.info(f'CROP {key} retrieved')
                    inference_results[key] = result_crop
                    inference_id_dict[key] = -1
                    crops_counter -= 1

        new_image_crops_list = []
        for index_image, image_crops in enumerate(input_data):
            new_image_crops = []
            new_image_crops_2 = []
            for index_crop, image_crop in enumerate(image_crops):
                new_image_crops.append(inference_results[(index_image, index_crop)])
                if hazard == 'Wildfire':
                    new_image_crops_2.append(inference_results_2[(index_image, index_crop)])
            new_image_crops_list.append(new_image_crops)
            if hazard == 'Wildfire':
                grading_map_resource = copy.deepcopy(raw_data_dict['resources'][0])
                raw_data_dict['resources'].append(grading_map_resource)
                new_image_crops_list.append(new_image_crops_2)
        logger.info("Inference results retrieved")
        return new_image_crops_list

    except Exception as e:
        logger.error(traceback.format_exc())
        raise e


def prepare_output(raw_data_dict: dict, image_crops_list: list) -> dict:
    logger.info('Reconstructing output images')
    try:
        images = []
        for idx, image_crops in enumerate(image_crops_list):
            if len(image_crops) == 0:
                continue
            image = get_image_from_crops(raw_data_dict['resources'][idx]['image'].shape[:2], image_crops)
            images.append(image)
        data_dict = raw_data_dict
        for i, item in enumerate(zip(data_dict['resources'], images)):
            resource, image = item
            resource['name_prefix'] = get_resource_name(raw_data_dict['hazard'], i)
            resource['image'] = image
        logger.info('Output images reconstructed')
        return data_dict
    except Exception as e:
        logger.error(traceback.format_exc())
        raise e


def save_on_buffer(hazard, image):
    buf = io.BytesIO()
    extension = None
    if hazard == 'Wildfire':
        extension = 'npy'
        np.save(buf, image)
        # np.save(f'img_{index_crop}.{get_format_from_hazard(hazard)}', img)
    if hazard == 'Flood':
        extension = 'png'
        data = Image.fromarray(image)
        # data.save(f'img_{index_crop}.{get_format_from_hazard(hazard)}', get_format_from_hazard(hazard))
        data.save(buf, extension)
    buf.seek(0)
    return buf, extension


def get_inference_request_data(hazard):
    if hazard == 'Wildfire':
        return None
    if hazard == 'Flood':
        return '''{"blur_kernel_size": %d}''' % 15


def get_resource_name(hazard, i):
    if hazard == 'Wildfire':
        if i % 2 == 0:
            return f'32002_LINKS_burned_area_delineation_{int(i / 2)}'
        if i % 2 == 1:
            return f'32003_LINKS_burned_area_severity_{int(i / 2)}'
    if hazard == 'Flood':
        return f'32001_LINKS_flood_delineation_{i}'
    return 'unknown_resource'
