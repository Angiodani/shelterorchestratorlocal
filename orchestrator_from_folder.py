import asyncio
import copy
import logging
import os
import signal
import sys
import time

import cv2
import tifffile

import numpy as np
from manager.datalake_manager import DataLakeManager
from manager.inference_manager import InferenceManager

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
logger = logging.getLogger(__name__)

inference_manager = InferenceManager()
data_lake_manager = DataLakeManager()


async def work(datum):
    try:
        raw_datum_after_event_dict = {
            'hazard': datum['hazard'],
            'resources': [datum]
        }

        uploaded_geospatial_image_info = await data_lake_manager.upload_data(copy.deepcopy(raw_datum_after_event_dict))
        result = await inference_manager.get_inference(copy.deepcopy(raw_datum_after_event_dict))
        uploaded_data_info = await data_lake_manager.upload_data(result, metadata_id=uploaded_geospatial_image_info[0]['package_id'])
        logger.info('Completed.')
        return uploaded_data_info
    except Exception:
        logger.error('Exception occurred during the message processing. Sending Failure message on bus.')


async def callback(folder):
    try:
        start = time.perf_counter()

        loop = asyncio.get_event_loop()
        tasks = []

        for filename in os.listdir(folder):
            if filename.split('.')[-1] in ['tif', 'tiff']:
                img = np.array(tifffile.imread(os.path.join(folder, filename)))
                img = (img * 255).astype(np.uint8)
            else:
                img = cv2.imread(os.path.join(folder, filename))
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            datum = {
                'title': 'Srbac, Bosnia and Herzegovina 2019-05-14',
                'notes': 'Flood delineation from folder 1-37700 GIULIO',
                'name_prefix': 'geospatial_image',
                'format': 'tiff',
                'data_temporal_extent_begin_date': '2014-11-08T00:00:00',
                'data_temporal_extent_end_date': '2014-11-08T00:00:00',
                "coordinate_system_id": 4326,
                "coordinate_reference_system": 'WGS84',
                'bbox': [16.18835, 45.528598, 16.407051, 45.431708],
                'hazard': 'Flood',
                'resolution': 10,
                'image': img
            }
            tasks.append(loop.create_task(work(datum)))

        def signal_handler(signal, frame):
            loop.stop()
            sys.exit(0)

        signal.signal(signal.SIGINT, signal_handler)
        results = await asyncio.gather(*(task for task in tasks))

        end = time.perf_counter() - start
        logger.info(f"Program finished in {end:0.2f} seconds.")
        for result in results:
            logger.info(result)
    except BaseException as e:
        logger.error('Exception encountered while handling the message: {}'.format(str(e)))
        # raise e


if __name__ == "__main__":
    folder = 'data_folder'
    asyncio.run(callback(folder))
