import asyncio
from datetime import datetime, timedelta

from manager.bus_manager import BusManager
from manager.datalake_manager import DataLakeManager


def callback(message):
    print(message)


# my_consumer = BusManager().get_consumer(callback)
# my_consumer.run()

manager = DataLakeManager()

start_date = datetime(2020, 1, 20, 0,0,0)
end_date = datetime.utcnow() + timedelta(-3)
result = asyncio.run(manager.get_data('5f72df29-0433-4f6f-8805-838f53966507'))
print(result['resources'])