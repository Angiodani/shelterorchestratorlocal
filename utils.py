import os
import shutil
import uuid

import numpy as np
import matplotlib.pyplot as plt
from osgeo import gdal, osr

from manager import settings

DEFAULT_CROP_SIZE = 480, 480
SENTINEL_HUB_CLIENT_ID = '44dc9a5b-708a-42f6-bfa3-657877deb22a'
SENTINEL_HUB_CLIENT_SECRET = '!w]&IFwOG[BO88d!cN|Vk<kc,.2Y&X2EF]HLV;xd'

DATA_LAKE_OAUTH_USER = '376a06f6-1229-4c94-b29d-d6cebbb471a0'
DATA_LAKE_OAUTH_PWD = '9XfB3gVvVVhjZTz2MbdvFfDZfoesLwCQcz90nXe9go0'
DATA_LAKE_OAUTH_API_KEY = 'T4_qfVVlPwtRS9m0cFogLcn-H20XvG41XmuGM9OVDGM'

'''
    Decompose the image in tiles of size "crop_size". Adjacent tiles have 2X "crop_margin" pixels overlapping, in order
    to avoid the bad predictions of convolutional models along the edges. "crop_margin" should be equal to the number
    of pixels one needs to move away from edges to have a faithful prediction
'''


def get_image_crops(image, crop_size=DEFAULT_CROP_SIZE, crop_margin: int = 4):
    image_crops = []
    h, w = image.shape[:2]
    for y in range(0, h, int(crop_size[0] - 2 * crop_margin)):
        condition_on_y = y + crop_size[0] < h
        # if condition_on_y is False, y component of crop_size exceeds the image height, hence the crop must be shifted
        # in such a way that y + crop_size[0] is exactly h
        y_from = y if condition_on_y else (h - crop_size[0])
        y_to = y + crop_size[0] if condition_on_y else None
        for x in range(0, w, int(crop_size[1] - 2 * crop_margin)):
            condition_on_x = x + crop_size[1] < w
            # same behaviour as before but on horizontal component
            x_from = x if condition_on_x else (w - crop_size[1])
            x_to = x + crop_size[1] if condition_on_x else None
            image_crop = np.array(image[y_from:y_to, x_from:x_to, :])
            # plot_image(image_crop)
            image_crops.append(image_crop)
    return image_crops


'''
    Recompose the original image from a list of tiles of size "crop_size". Adjacent tiles have 2X "crop_margin" pixels
    overlapping, hence we drop the last "crop_margin" pixels from the upper-left-most and the first "crop_margin" pixels
    from the lower-right-most tile
'''


def get_image_from_crops(image_shape: tuple, image_crops: list, crop_size=DEFAULT_CROP_SIZE, crop_margin: int = 4):
    image = np.zeros(image_shape)
    h, w = image.shape[:2]
    counter = 0
    for y in range(0, h, int(crop_size[0] - 2 * crop_margin)):
        condition_on_y = y + crop_size[0] < h
        # if condition_on_y is False, y component of crop_size exceeds the image height, hence this means that when
        # tiling, the crop was shifted in such a way that y + crop_size[0] was exactly h. Now we reconstruct this tile
        # in that position
        y_from = None if y == 0 else (y + crop_margin if condition_on_y else h - crop_size[0] + crop_margin)
        y_to = y + crop_size[0] - crop_margin if condition_on_y else None
        for x in range(0, w, int(crop_size[1] - 2 * crop_margin)):
            condition_on_x = x + crop_size[1] < w
            x_from = None if x == 0 else (x + crop_margin if condition_on_x else w - crop_size[1] + crop_margin)
            x_to = x + crop_size[1] - crop_margin if condition_on_x else None
            image[y_from:y_to, x_from:x_to, ...] = image_crops[counter][
                                                   None if y_from is None else crop_margin:
                                                   None if y_to is None else int(crop_size[0] - crop_margin),
                                                   None if x_from is None else crop_margin:
                                                   None if x_to is None else int(crop_size[1] - crop_margin)
                                                   ]
            counter += 1
    return image


def plot_image(image, factor=1):
    """
    Utility function for plotting RGB images.
    """
    fig = plt.subplots(nrows=1, ncols=1, figsize=(15, 7))

    if np.issubdtype(image.dtype, np.floating):
        plt.imshow(np.minimum(image * factor, 1))
    else:
        plt.imshow(image)
    plt.show()


def save_array_as_geotiff(array, bbox, coordinate_system_id, filename) -> str:
    filename = get_destination_dir(filename)
    ny, nx = array.shape[:2]
    if isinstance(bbox, dict):
        # If we are in this case, we use the coordinates of the bounding box of the map request
        xmin, ymin = bbox['coordinates'][0][0]
        xmax, ymax = bbox['coordinates'][0][2]
        xres = (xmax - xmin) / float(nx)
        yres = (ymax - ymin) / float(ny)
    else:
        # If we are in this case, we have access to the actual coordinates of the satellite image, that may differ
        # from the ones indicated in the map request (even if in the average case they should be equal)
        assert isinstance(bbox, list)
        xmin, ymin, xres, yres = bbox
    geotransform = (xmin, xres, 0, ymin, 0, -yres)
    # create the N-band raster file
    bands = 1 if len(array.shape) == 2 else array.shape[2]
    dst_ds = gdal.GetDriverByName('GTiff').Create(filename, nx, ny, bands, gdal.GDT_Byte)

    dst_ds.SetGeoTransform(geotransform)  # specify coords
    srs = osr.SpatialReference()  # establish encoding
    srs.ImportFromEPSG(coordinate_system_id)  # 4326 is WGS84 lat/long
    dst_ds.SetProjection(srs.ExportToWkt())  # export coords to file
    if bands == 1:
        dst_ds.GetRasterBand(1).WriteArray(array)  # write band to the raster
    else:
        for band in range(bands):
            dst_ds.GetRasterBand(band + 1).WriteArray(array[:, :, band])  # write N-band to the raster
    dst_ds.FlushCache()  # write to disk
    dst_ds = None
    del dst_ds

    return filename


def get_destination_dir(filename):
    if not os.path.exists(settings.temp_folder):
        os.makedirs(settings.temp_folder)
    resource_temp_folder_name = settings.temp_folder + f'{uuid.uuid4()}\\'
    if not os.path.exists(resource_temp_folder_name):
        os.makedirs(resource_temp_folder_name)
    filename = resource_temp_folder_name + filename
    return filename


def clear_dir(path_to_dir):
    shutil.rmtree(path_to_dir, ignore_errors=True)
