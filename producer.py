from manager.bus_manager import BusManager

# Resolution [Optional] default 20

MESSAGE = '''
{
    "Title": "Kutina",
    "Notes": "Flood delineation",
	"EventDelineationArea": {
		"Geography": {
			"CoordinateSystemId": 4326,
			"CoordinateReferenceSystem": "WGS84",
			"Resolution": 20,
			"BBox": {
			    "type": "Polygon",
                "coordinates": [
                    [[16.8791, 45.2966], [16.4012, 45.2966], [16.4012, 45.5391], [16.8791, 45.5391], [16.8791, 45.2966]]
                ]
			}
		}
	},
	"EventDelineationTime": "2018-03-25",
	"EventDelineationTimeMaxDelta": 7,
	"Hazard": "Flood",
	"RequestCode": "mr000000040"
}
'''

################## PRODUCER FOR TESTING PURPOSES ##################

my_producer = BusManager().get_input_producer()
my_producer.publish(MESSAGE)

# [[105.92, 12.18], [105.96, 12.18], [105.96, 12.23], [105.92, 12.23], [105.92, 12.18]]
###################################################################
# NO DATA [16.4800, 45.5160, 16.7840, 45.3734] - Bobovak, Croazia 2015-10-16 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR142
# [16.1260, 45.5990, 16.6175, 45.4280] - Lijevi Dubrovcak, Croazia 2018-03-22 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR275
# [16.4012, 45.5391, 16.8791, 45.2966] - Kutina, Croazia 2018-03-25 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR275
# [16.7660, 45.3176, 17.1030, 45.1714] - Novska, Croazia 2018-03-25 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR275
# [16.3092, 45.2512, 16.8832, 44.8320] - Prijedor, Bosnia and Herzegovina 2019-05-15 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR358
# [16.6288, 44.8345, 16.7066, 44.7312] - Sanski, Bosnia and Herzegovina 2019-05-15 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR358
# [18.1945, 45.1537, 18.7667, 44.9862] Samac, Bosnia and Herzegovina 2019-05-16 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR358
# [17.4133, 45.1545, 17.6360, 45.0050] Srbac, Bosnia and Herzegovina 2019-05-16 - https://emergency.copernicus.eu/mapping/list-of-components/EMSR358

# [-6.9301, 42.5494, -6.8407, 42.5016] Portela de Aguiar, Spain 2017-10-22 - Wildfire
# [43.4109, 41.8468, 43.5340, 41.7836] Tsaghveri, Georgia 2017-08-28 - Wildfire
# [12.8966, 52.1048, 13.0758, 51.9950] Bardenitz, Germany 2018-07-31 - Wildfire
# [-7.545126168186687, 41.891104767380234, -7.48038073849681, 41.952261199521544] Luogo Sconosciuto 2017-07-30 - Wildfire

'''
MONITORING 1
142 Bobovac  - [16.3660, 45.5699, 16.9079, 45.2895]
142 Sisak    - [15.9418, 45.5677, 16.4837, 45.2889]
142 Karlovac - [15.3900, 45.846, 16.6723, 45.1922]
275 Lijevi Dubrovcak - [16.1348, 45.7034, 16.618144, 45.42509]
275 Kutina   - [16.3751, 45.5615, 16.9784, 45.2476]
275 Novska   - [16.7420, 45.3929, 17.3423, 45.0774]
'''

# FLOOD

''' 2 date immagini, 1 con alluvione e 1 senza, quella con è scentrata
{
    "Title": "Sanski, Bosnia and Herzegovina 2019-05-15",
    "Notes": "Flood delineation",
	"EventDelineationArea": {
		"Geography": {
			"CoordinateSystemId": 4326,
			"CoordinateReferenceSystem": "WGS84",
			"Resolution": 20,
			"BBox": {
			    "type": "Polygon",
                "coordinates": [
                    [[16.6288, 44.8345], [16.7066, 44.8345], [16.7066, 44.7312], [16.6288, 44.7312], [16.6288, 44.8345]]
                ]
			}
		}
	},
	"EventDelineationTime": "2019-05-15",
	"EventDelineationTimeMaxDelta": 7,
	"Hazard": "Flood",
	"RequestCode": "mr000000040"
}
'''

'''
{
    "Title": "Kutina",
    "Notes": "Flood delineation",
	"EventDelineationArea": {
		"Geography": {
			"CoordinateSystemId": 4326,
			"CoordinateReferenceSystem": "WGS84",
			"Resolution": 20,
			"BBox": {
			    "type": "Polygon",
                "coordinates": [
                    [[16.3751, 45.5615], [16.9784, 45.5615], [16.9784, 45.2476], [16.3751, 45.2476], [16.3751, 45.5615]]
                ]
			}
		}
	},
	"EventDelineationTime": "2018-03-25",
	"EventDelineationTimeMaxDelta": 7,
	"Hazard": "Flood",
	"RequestCode": "mr000000040"
}
'''

'''
{
    "Title": "Prijedor, Bosnia and Herzegovina 2019-05-15",
    "Notes": "Flood delineation",
	"EventDelineationArea": {
		"Geography": {
			"CoordinateSystemId": 4326,
			"CoordinateReferenceSystem": "WGS84",
			"Resolution": 20,
			"BBox": {
			    "type": "Polygon",
                "coordinates": [
                    [[16.3092, 45.2512], [16.8832, 45.2512], [16.8832, 44.8320], [16.3092, 44.8320], [16.3092, 45.2512]]
                ]
			}
		}
	},
	"EventDelineationTime": "2019-05-15",
	"EventDelineationTimeMaxDelta": 7,
	"Hazard": "Flood",
	"RequestCode": "mr000000040"
}
'''

# WILDFIRE
'''
{
    "Title": "Tsaghveri, Georgia 2017-08-28",
    "Notes": "Wildfire delineation",
	"EventDelineationArea": {
		"Geography": {
			"CoordinateSystemId": 4326,
			"CoordinateReferenceSystem": "WGS84",
			"Resolution": 20,
			"BBox": {
			    "type": "Polygon",
                "coordinates": [
                    [[43.4109, 41.8468], [43.5340, 41.8468], [43.5340, 41.7836], [43.4109, 41.7836], [43.4109, 41.8468]]
                ]
			}
		}
	},
	"EventDelineationTime": "2017-08-28",
	"EventDelineationTimeMaxDelta": 7,
	"Hazard": "Wildfire",
	"RequestCode": "mr000000040"
}
'''
